FROM hayd/alpine-deno:1.3.3

# Expose nginx port
EXPOSE 80

WORKDIR /app

COPY src src
COPY static static
RUN deno cache src/main.ts
RUN ls -la .

# We use nginx as proxy in order to get gzip compression and correct caching for static files
RUN apk add nginx
COPY docker/nginx.default.conf /etc/nginx/conf.d/default.conf
COPY docker/docker-redis-nginx.sh .

CMD ["./docker-redis-nginx.sh"]
