# AcroWatson

Acrolinx App using [IBM Watson Tone Analyzer](https://www.ibm.com/watson/services/tone-analyzer/).

Works also as standalone web app.
Please enjoy a [Demo](https://acrowatson.fly.dev/).

## How to Start Locally for Development

Install [Deno](https://deno.land/#installation).

Configure the Watson Tone Analyzer credentials in the environment variables
WATSON_API_KEY and WATSON_API_URL. 

```bash
deno run --allow-net --allow-env --allow-read src/main.ts
```

## Run inside Docker using Docker Compose

Install [Docker Compose](https://docs.docker.com/compose/install).

Configure the Watson Tone Analyzer credentials in the environment variables
WATSON_API_KEY and WATSON_API_URL (for examle in a .env file based on .template.env). 

Run:
```
docker-compose up --build
```

## Deployment to fly.io

Install [Fly](https://fly.io/docs/getting-started/installing-flyctl/)

Configure:
```bash
fly secrets set  WATSON_API_KEY=key WATSON_API_URL=url
```

Deploy
```bash
fly deploy
```

## Links

* https://cloud.ibm.com/apidocs/tone-analyzer
* https://cloud.ibm.com/services/tone-analyzer/crn
* https://cloud.ibm.com/

## License
 
MIT
 
## Copyright

Copyright (c) 2020 Marco Stahl 

