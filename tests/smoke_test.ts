import {assertStringContains} from 'https://deno.land/std@0.70.0/testing/asserts.ts';

Deno.test('Smoke test of an already running server', async () => {
  const html = await fetch('http://localhost:8080/?extractedText=I+love+it.+This+is+better+then+before.').then(it => it.text());
  assertStringContains(html, 'AcroWatson');
  assertStringContains(html, 'I love it');
  assertStringContains(html, 'This is better then before.');
  assertStringContains(html, 'Joy');
  assertStringContains(html, 'Analytical');
});

