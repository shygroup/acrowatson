# Workaround for https://github.com/gliderlabs/docker-alpine/issues/185
# Works on fly.io only in this shell script but not in the Dockerfile, weird.
mkdir -p /run/nginx
nginx
deno run --allow-net --allow-env --allow-read src/main.ts
