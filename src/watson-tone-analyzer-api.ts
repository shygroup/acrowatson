export interface DocumentAnalysis {
  document_tone: {
    tones: ToneScore[];
  };
  sentences_tone?: SentenceAnalysis[]
}

export interface ToneScore {
  score: number; // 0.5 to 1
  tone_id: string;
  tone_name: string;
}

export interface SentenceAnalysis {
  sentence_id: number;
  text: string;
  tones: ToneScore[];
}

export interface ApiCredentials {
  apikey: string;
  url: string;
}

export async function analyzeDocument(credentials: ApiCredentials, text: string): Promise<DocumentAnalysis> {
  const result = await fetch(credentials.url + '/v3/tone?version=2017-09-21', {
    method: 'POST',
    headers: {
      Authorization: 'Basic ' + btoa('apikey:' + credentials.apikey),
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({text})
  }).then(it => it.json());

  if (result.error) {
    console.error('Error while analyzing document', result);
    throw new Error(result.error + ' ' + result.code)
  }

  return result;
}
