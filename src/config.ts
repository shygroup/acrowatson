import {ApiCredentials} from './watson-tone-analyzer-api.ts';

const watsonApiUrl = Deno.env.get('WATSON_API_URL');
if (!watsonApiUrl) {
  throw new Error('Missing environment variable WATSON_API_URL');
}

const watsonApiKey = Deno.env.get('WATSON_API_KEY');
if (!watsonApiKey) {
  throw new Error('Missing environment variable WATSON_API_KEY');
}

export const WATSON_API_CREDENTIALS: ApiCredentials = {
  apikey: watsonApiKey,
  url: watsonApiUrl
};
