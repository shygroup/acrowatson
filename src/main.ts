import {serve} from 'https://deno.land/std/http/server.ts';
import {WATSON_API_CREDENTIALS} from './config.ts';
import {DUMMY_DOCUMENT_ANALYSIS} from './dummy-result.ts';
import {serveStaticFile} from './utils/serve-files.ts';
import {escapeHtml} from './utils/utils.ts';
import {analyzeDocument, ApiCredentials, SentenceAnalysis, ToneScore} from './watson-tone-analyzer-api.ts';

async function main() {
  console.log('Starting server at http://localhost:8080');
  for await (const req of serve({port: 8080})) {
    try {
      const [urlPath, searchParamsString] = req.url.split('?');
      if (urlPath === '/') {
        req.respond({
          headers: new Headers({'Content-Type': 'text/html; charset=utf-8'}),
          body: await renderPage(new URLSearchParams(searchParamsString))
        });
      } else {
        await serveStaticFile(req);
      }
    } catch (error) {
      if (error instanceof Deno.errors.NotFound) {
        req.respond({body: 'Not found', status: 404})
      } else {
        console.error('Error while handling request', error);
        req.respond({body: 'Error: ' + error.message, status: 500});
      }
    }
  }
}

export async function renderPage(searchParams: URLSearchParams) {
  const text = (searchParams.get('extractedText') ?? '').replace(/\r\n/g, '\n');

  if (text.trim() === '') {
    return renderPageHtml('');
  }

  const analysis = await analyzeDocument(WATSON_API_CREDENTIALS, text);
  //const analysis = DUMMY_DOCUMENT_ANALYSIS;

  const documentToneScores = sortToneScoresDesc(analysis.document_tone.tones);
  const sentences = analysis.sentences_tone && extendSentenceAnalysisWithOffsets(text,
    analysis.sentences_tone.filter(sentence => sentence.tones.length >= 1)
  );

  return renderPageHtml(text, documentToneScores, sentences);
}

interface SentenceAnalysisWithOffsets extends SentenceAnalysis {
  begin: number;
  end: number;
}

function extendSentenceAnalysisWithOffsets(text: string, sentenceAnalysis: SentenceAnalysis[]): SentenceAnalysisWithOffsets[] {
  return sentenceAnalysis.map(sentence => {
    const begin = text.indexOf(sentence.text); // Let's assume every sentence is unique.
    return {
      ...sentence,
      tone: sortToneScoresDesc(sentence.tones),
      begin,
      end: begin + sentence.text.length,
    };
  })
}

function sortToneScoresDesc(toneScores: ToneScore[]) {
  return [...toneScores].sort((a, b) => b.score - a.score);
}

function renderPageHtml(extractedText: string, documentToneScores?: ToneScore[], sentences?: SentenceAnalysisWithOffsets[]) {
  return `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>AcroWatson</title>
    <link rel="stylesheet" href="style.css">
    <script src="acrolinx-app-sdk-auto-form.min.js"></script>
    <meta name="acrolinx-app-title" content="AcroWatson" />
    <meta name="acrolinx-app-button-text" content="Analyze Tone" />
    <meta name="acrolinx-app-button-tooltip" content="Analyze the Tone of your Document" />
    <meta name="acrolinx-app-required-commands" content="selectRanges, replaceRanges" />
    <meta name="acrolinx-app-required-events" content="invalidRanges" />
    <meta name="acrolinx-app-invalid-range-tooltip" content="This sentence is out of date. Your document has changed since we found this sentence. Try checking again." />
</head>

<body>
    <h1>AcroWatson</h1>
    
    <form data-acrolinx="hide" method="get">
        <label for="extractedText">Text:</label>
        <textarea id="extractedText" name="extractedText" data-acrolinx="extractedText" autofocus>${escapeHtml(extractedText)}</textarea>
        <p><input type="submit"></p>
    </form>
    
    ${documentToneScores ? '<h2>Tone of Document</h2>' + renderToneScoresTable(documentToneScores) : '<div class="message">Give me some text!</div>'}
    ${documentToneScores && documentToneScores.length === 0 ? 'Your document has no tone at all. Please write something more emotional!' : ''}
    ${sentences ? '<h2>Tone of Sentences</h2>' + renderSentences(sentences) : ''}
    
</body>
</html>
`;
}

function renderSentences(sentences: SentenceAnalysisWithOffsets[]) {
  return `
    <div class="sentences">
      ${sentences.map(renderSentence).join('')}
    </div>
  `;
}

function renderSentence(sentence: SentenceAnalysisWithOffsets) {
  return `
    <div class="sentence" data-acrolinx-select-range='{"begin": ${sentence.begin}, "end": ${sentence.end}}'>
      <div class="sentenceTop">
        <div class="sentenceText"> 
          ${sentence.text}
        </div>
        <div class="strongestToneLabel">${sentence.tones[0]?.tone_name}</div>
      </div>
      <div class="sentenceDetails">
        ${renderToneScoresTable(sentence.tones)}
      </div>
    </div>
  `;
}

function renderToneScoresTable(toneScores: ToneScore[]) {
  return `
    <table>
      ${toneScores.map(toneScore => `
        <tr>
          <th>${toneScore.tone_name}</th>        
         <td>${(Math.round(toneScore.score * 100))}</td>        
       </tr>
      `).join('')}
    </table>
  `;
}

main();
